# Interactive Optimization of Embedding-based Ensembles

![Screenshot of the EEVO application](/misc/eevo.png "")

This project contains the code of the application presented in the papers [Interactive Optimization of Embedding-based Text Similarity Calculations](https://journals.sagepub.com/doi/full/10.1177/14738716221114372) and [Visually Guided Network Reconstruction Using Multiple Embeddings](https://urn.kb.se/resolve?urn=urn:nbn:se:bth-24445)

The application is implemented with HTML5/JavaScript/D3 and contains two application files: eevo.html and eevo.css. The data files (plain text files) are located in the separate data folder. The application needs to be served via a web server for the data loading to work, and it also needs internet connection for D3.

## Launch instructions

1. Create a new main folder for the application

2. Copy the two application files to the main folder

3. In the main folder: create a sub folder named "data" and copy all the data files to it

4. Run a web server on/from the main folder

5. Navigate a browser (preferably Chrome) to eevo.html
